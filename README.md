VM-defaults
=========

Role prepares system freshly installed CentOS system with most common
tools and configuration. It'll also install filebeat and metricbeat
if appropriate variables are defined.

Role Variables
--------------

// TODO:


Example Playbook
----------------

Example playbook and inventory file is provided in ./test directory. Testing of this role
can be executed using Vagrant. Position yourself to root directory of role and execute
`vagrant up` command.

License
-------

Apache License, Version 2.0

Author Information
------------------
Matija Bartolac  
CROZ d.o.o  
mbartolac@croz.net  
gitlab.com/kdmk  
  
You can find more roles at gitlab.com/anisble-roles
